SELECT * FROM public.projects
ORDER BY id ASC;

insert into public.projects (name,description)
values 
('DA Job Search','The project for searching a job as Data Analyst EU'),
('DS Job Search','The project for searching a job as Data Scientist in EU'),
('BIE Job Search','The project for searching a job as Business Intelligence Engineer in EU');

insert into public.projects (name,description)
values 
('Software dev Job Search','The project for searching a job as Software Developer in EU');

insert into prj_assignments values
(1,2),(2,3),(3,1);

SELECT * FROM public.blogpost
ORDER BY id ASC;

-- Exercise 1
select
b.title
,b.content
,b.published
,b.created_at
,w.url as published_on
,u.email as blog_creator
from links l
right join blogpost b
on  l.post_id = b.id
left join public.user u
on b.writer_id = u.id
left join website w
on l.website_id = w.id;

-- Excercise 2
select
e.first_name || ' ' || e.last_name as emp_name
,c.name as company_name
,c.url as company_website
,p.name as project_name
,p.description as project_description
from prj_assignments pa
right join employee e
on pa.user_id = e.id
join companies c
on e.company_id = c.id
left outer join projects p
on pa.project_id = p.id;